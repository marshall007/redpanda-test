#!/bin/bash

set -e

# create kafka topic
docker exec -it redpanda \
  rpk topic create github --brokers=localhost:9092

mkdir -p data
if [ ! -f ./data/github_all_columns.ndjson ]; then
  wget -qO- "https://datasets-documentation.s3.amazonaws.com/kafka/github_all_columns.ndjson" > ./data/github_all_columns.ndjson
fi

# load data into topic
docker run -it --rm --network=host --volume "$(pwd)/data:/data" confluentinc/cp-kafkacat \
  kafkacat -b localhost:9092 -t github -P -l /data/github_all_columns.ndjson

# create destination table
docker exec -it clickhouse clickhouse-client \
  --query "$(cat queries/01-create-table.sql)"

# create table using kafka engine
docker exec -it clickhouse clickhouse-client \
  --query "$(cat queries/02-create-stream.sql)"

# create materialized view
docker exec -it clickhouse clickhouse-client \
  --query "$(cat queries/03-create-view.sql)"

# # query data
# docker exec -it clickhouse clickhouse-client \
#   --query "$(cat queries/04-select.sql)"

echo "view data in clickhouse playground:"
echo "http://localhost:8123/play#$(cat queries/04-select.sql | base64)"
